package hbgamestates;

import java.awt.Graphics;

import hbgame.Game;
import hbgame.Handler;
import hbgameentities.Fruit;
import hbgameentities.Player;
import hbgamegfx.Assets;
import hbgametiles.Tile;
import hbgameworlds.World;

public class GameState extends State{
	
	private Player player;
	private World world;
	private Fruit fruit;

	public GameState(Handler handler) {
		super(handler);
		
		world = new World(handler, "./res/world/worldData.txt");
//		world = new World(handler, "/home/hugo/hugo/Studies/oo/ep2/Snake-EP2/res/world/worldData.txt");
		handler.setWorld(world);
		player = new Player(handler, 128, 224);
		fruit = new Fruit(handler, player, 0, 0, 32, 32);
		
	}
	
	@Override
	public void tick() {
		
		fruit.tick();
		world.tick();
		player.tick();
		
		if(player.isGameOver() == true) {
			State.setState(new GameOverState(handler));
		}
		
		handler.getGame().finalScore = player.getScore();
		
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	@Override
	public void render(Graphics g) {
		
		world.render(g);
		fruit.render(g);
		player.render(g);
		
	}
}
