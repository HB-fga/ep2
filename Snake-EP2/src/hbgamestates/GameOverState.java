package hbgamestates;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import hbgame.Handler;
import hbgameentities.Player;
import hbgamegfx.Animation;
import hbgamegfx.Assets;

public class GameOverState extends State {
	
	Animation seaAnimation;
	
	public GameOverState(Handler handler) {
		super(handler);
		
		seaAnimation = new Animation(100, Assets.sea);
	}

	@Override
	public void tick() {
		
		seaAnimation.tick();
		
		if(handler.getKeyManager().m) {
			
			//RANKING
			
			State.setState(new MenuState(handler));
		}
		if(handler.getKeyManager().r)
			State.setState(new GameState(handler));
	}

	@Override
	public void render(Graphics g) {
		for(int i = 0;i < 15;i++) {
			for(int j = 0;j < 25;j++) {
				g.drawImage(seaAnimation.getCurrentFrame(), j * 32, i * 32,  null);
			}
		}
		
		g.setColor(Color.BLACK);
		
		g.fillRect(195, 145, 390, 210);
		
		g.setColor(Color.WHITE);
		g.setFont(new Font("arial", Font.BOLD, 50));
		g.drawString("Game Over",  235, 200);
		g.setFont(new Font("arial", Font.BOLD, 20));
		g.drawString("Press 'R' to retry",  300, 280);
		g.drawString("Press 'M' to go back to the menu",  203, 310);
		g.drawString("Final Score: " + Long.toString(handler.getGame().finalScore),  315, 340);
		
//		g.drawString("Press 'Enter' to register your name",  190, 300);
//		g.drawString("and go back to the menu",  245, 320);
	}

	@Override
	public Player getPlayer() {
		return null;
	}

}
