package hbgamestates;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import hbgame.Handler;
import hbgameentities.Player;
import hbgamegfx.Animation;
import hbgamegfx.Assets;

public class CreditsState extends State {

	Animation seaAnimation;
	
	public CreditsState(Handler handler) {
		super(handler);
		
		seaAnimation = new Animation(100, Assets.sea);
	}

	@Override
	public void tick() {
		seaAnimation.tick();
		
		if(handler.getKeyManager().m)
			State.setState(new MenuState(handler));
	}

	@Override
	public void render(Graphics g) {
		for(int i = 0;i < 15;i++) {
			for(int j = 0;j < 25;j++) {
				g.drawImage(seaAnimation.getCurrentFrame(), j * 32, i * 32,  null);
			}
		}
		
		g.setColor(Color.BLACK);
		
		g.fillRect(5, 5, 275, 395);
		
		g.fillRect(310, 440, 500, 100);
		
		g.setColor(Color.WHITE);
		
		g.setFont(new Font("arial", Font.BOLD, 50));
		g.drawString("Credits",  10, 60);
		
		g.setFont(new Font("arial", Font.BOLD, 25));
		g.drawString("Sea Tile Animation",  10, 120);
		g.setFont(new Font("arial", Font.BOLD, 18));
		g.drawString("Jason Wild",  10, 140);
		
		g.setFont(new Font("arial", Font.BOLD, 25));
		g.drawString("Everything else",  10, 195);
		g.setFont(new Font("arial", Font.BOLD, 18));
		g.drawString("Hugo Bezerra",  10, 215);
		
		g.setFont(new Font("arial", Font.BOLD, 25));
		g.drawString("Special thanks to",  10, 270);
		g.setFont(new Font("arial", Font.BOLD, 18));
		g.drawString("Nicole",  10, 290);
		g.drawString("Xavier",  10, 310);
		g.drawString("Kabio",  10, 330);
		g.drawString("Eric",  10, 350);
		g.drawString("Vini",  10, 370);
		g.drawString("Lucas B.",  10, 390);
		
		g.setFont(new Font("arial", Font.BOLD, 25));
		g.drawString("Press 'M' to go back to the menu",  315, 470);
	}

	@Override
	public Player getPlayer() {
		return null;
	}

}
