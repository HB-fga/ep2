package hbgamestates;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import hbgame.Handler;
import hbgameentities.Player;
import hbgamegfx.Animation;
import hbgamegfx.Assets;

public class TutorialState extends State {
	
	Animation seaAnimation;
	Animation boatAnimation;
	
	public TutorialState(Handler handler) {
		super(handler);
		
		seaAnimation = new Animation(100, Assets.sea);
		boatAnimation = new Animation(200, Assets.boatRight);
	}

	@Override
	public void tick() {
		
		seaAnimation.tick();
		boatAnimation.tick();
		
		if(handler.getKeyManager().m) {
			State.setState(new MenuState(handler));
		}
	}

	@Override
	public void render(Graphics g) {
		for(int i = 0;i < 15;i++) {
			for(int j = 0;j < 25;j++) {
				g.drawImage(seaAnimation.getCurrentFrame(), j * 32, i * 32,  null);
			}
		}
		
		g.setColor(Color.BLACK);
		
		g.fillRect(15, 15, 770, 450);
		
		g.setColor(Color.WHITE);
		g.setFont(new Font("arial", Font.BOLD, 50));
		g.drawString("Tutorial",  280, 70);
		
		g.setFont(new Font("arial", Font.BOLD, 15));
		g.drawString("Press 'M' to go back to the menu",  250, 95);
		
		g.drawImage(boatAnimation.getCurrentFrame(), 25, 110, 64, 64, null);
		g.drawString("You are boatman and need to help",  110, 130);
		g.drawString("the people drowning on the sea",  110, 150);
		g.drawString("Move with 'WASD' or Arrow keys",  110, 170);
		
		g.drawImage(Assets.star, 25, 200, 64, 64, null);
		g.drawString("Turns your boat into the Star Boat",  110, 220);
		g.drawString("which doubles the score you get",  110, 240);
		g.setFont(new Font("arial", Font.ITALIC, 15));
		g.drawString("AKA 'Star Snake'",  110, 260);
		
		g.setFont(new Font("arial", Font.BOLD, 15));
		g.drawImage(Assets.kitty, 25, 290, 64, 64, null);
		g.drawString("Turns your boat into the Kitty Boat",  110, 310);
		g.drawString("which makes you ignore rock walls",  110, 330);
		g.setFont(new Font("arial", Font.ITALIC, 15));
		g.drawString("AKA 'Kitty Snake'",  110, 350);
		
		g.setFont(new Font("arial", Font.BOLD, 15));
		g.drawImage(Assets.portal[0], 25, 380, 64, 64, null);
		g.drawString("Turns your boat into the Teleport",  110, 400);
		g.drawString("Boat which teleports you (oh really?)",  110, 420);
		g.setFont(new Font("arial", Font.ITALIC, 15));
		g.drawString("AKA 'Atravessar bordas do jogo'",  110, 440);
		
		g.setFont(new Font("arial", Font.BOLD, 15));
		g.drawImage(Assets.help[0], 440, 110, 64, 64, null);
		g.drawString("Normal guy",  530, 130);
		g.drawString("Gives 1 score point",  530, 150);
		g.setFont(new Font("arial", Font.ITALIC, 15));
		g.drawString("AKA 'Simple Fruit'",  530, 170);
		
		g.setFont(new Font("arial", Font.BOLD, 15));
		g.drawImage(Assets.dHelp[0], 440, 200, 64, 64, null);
		g.drawString("Fat guy",  530, 220);
		g.drawString("Gives 2 score points",  530, 240);
		g.setFont(new Font("arial", Font.ITALIC, 15));
		g.drawString("AKA 'Big Fruit'",  530, 260);
		
		g.setFont(new Font("arial", Font.BOLD, 15));
		g.drawImage(Assets.shrink, 440, 290, 64, 64, null);
		g.drawString("Lifeboat",  530, 310);
		g.drawString("Returns the people rescued",  530, 330);
		g.setFont(new Font("arial", Font.ITALIC, 15));
		g.drawString("AKA 'Decrease Fruit'",  530, 350);
		
		g.setFont(new Font("arial", Font.BOLD, 15));
		g.drawImage(Assets.bomb, 440, 380, 64, 64, null);
		g.drawString("Bomb",  530, 400);
		g.drawString("Kills you!",  530, 420);
		g.setFont(new Font("arial", Font.ITALIC, 15));
		g.drawString("AKA 'Bomb Fruit'",  530, 440);
	}

	@Override
	public Player getPlayer() {
		return null;
	}

}