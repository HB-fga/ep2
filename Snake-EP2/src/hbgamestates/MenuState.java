package hbgamestates;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import hbgame.Game;
import hbgame.Handler;
import hbgameentities.Player;
import hbgamegfx.Animation;
import hbgamegfx.Assets;

public class MenuState extends State {
	
	Animation seaAnimation;

	public MenuState(Handler handler) {
		super(handler);
		
		seaAnimation = new Animation(100, Assets.sea);
	}
	
	@Override
	public void tick() {
		seaAnimation.tick();
		
		if(handler.getKeyManager().enter)
			State.setState(new GameState(handler));
//		if(handler.getKeyManager().r)
//			State.setState(new MenuState(handler));
		if(handler.getKeyManager().c)
			State.setState(new CreditsState(handler));
		if(handler.getKeyManager().t)
			State.setState(new TutorialState(handler));
	}

	@Override
	public void render(Graphics g) {
		for(int i = 0;i < 15;i++) {
			for(int j = 0;j < 25;j++) {
				g.drawImage(seaAnimation.getCurrentFrame(), j * 32, i * 32,  null);
			}
		}
		
		g.drawImage(Assets.logo, 37, 50, null);
		
		g.setColor(Color.BLACK);
		
		g.fillRect(55, 265, 600, 120);
		
//		with Ranking option
//		g.fillRect(55, 265, 600, 155);
		
		g.setColor(Color.WHITE);
		g.setFont(new Font("arial", Font.BOLD, 30));
		g.drawString("Press 'Enter' to play!",  60, 300);
		g.drawString("Press 'T' to read the tutorial",  60, 335);
		g.drawString("Press 'C' to read the credits",  60, 370);
//		g.drawString("Press 'R' to view the leaderboard",  60, 405);
	}

	@Override
	public Player getPlayer() {
		return null;
	}

}
