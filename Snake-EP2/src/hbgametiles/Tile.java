package hbgametiles;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import hbgamegfx.Animation;
import hbgamegfx.Assets;

public class Tile {
	
	public static final int WIDTH = 32, HEIGHT = 32;
	
	public static Tile[] tiles = new Tile[256];
	public static Tile seaTile = new SeaTile(0);
	public static Tile wallTile = new WallTile(1);
	
	protected BufferedImage texture;
	protected final int id;
	
	public Tile(BufferedImage texture, int id) {
		this.texture = texture;
		this.id = id;
		
		tiles[id] = this;
	}
	
	public void tick() {
		
	}
	
	public void render(Graphics g, int x, int y) {
		g.drawImage(texture, x, y, WIDTH, HEIGHT, null);
	}
	
	public boolean isSafe() {
		return true;
	}
	
	public int getId() {
		return id;
	}
}
