package hbgametiles;

import hbgamegfx.Assets;

public class SeaTile extends Tile{

	public SeaTile(int id) {
		super(Assets.sea[0], id);
	}

}
 