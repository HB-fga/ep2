package hbgametiles;

import java.awt.image.BufferedImage;

import hbgamegfx.Assets;

public class WallTile extends Tile{

	public WallTile(int id) {
		super(Assets.wall, id);
	}
	
	@Override
	public boolean isSafe() {
		return false;
	}

}
