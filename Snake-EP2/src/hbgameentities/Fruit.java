package hbgameentities;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;

import hbgame.Handler;
import hbgamegfx.Animation;
import hbgamegfx.Assets;

public class Fruit extends Entity{

	/*
	 * normal fruit = 1
	 * star fruit = 2
	 * kitty fruit = 3
	 * teleport fruit = 4
	 * double fruit = 5
	 * decrease fruit = 6
	 * bomb fruit = 7
	 */
	
	// 25x15 board
	protected ArrayList<Integer> possibleSpawnPos;
	protected Player player;
	protected Random random;
	protected Animation aFruit;
	protected Animation aDFruit;
	protected Animation aPortal;
	
	protected int fruitType;
	protected boolean fruitCollision = false;
	protected long lastTime, timer;
	private final int fruitTimer = 12000; // 12 seconds
	
	public Fruit(Handler handler, Player player, float x, float y, int width, int height) {
		super(handler, x, y, width, height);
		this.player = player;
		possibleSpawnPos = new ArrayList<Integer>();
		random = new Random();
		
		int aux = generateSpawnPos();
		this.x =(float) (aux % 25) * Entity.WIDTH;
		this.y =(float) (aux / 25) * Entity.WIDTH;
		
		aux = random.nextInt(100);
		if(aux < 50) {
			this.fruitType = 1;
		}else if(aux < 59) {
			this.fruitType = 2;
		}else if(aux < 68) {
			this.fruitType = 3;
		}else if(aux < 76) {
			this.fruitType = 4;
		}else if(aux < 84) {
			this.fruitType = 5;
		}else if(aux < 92) {
			this.fruitType = 6;
		}else if(aux < 100) {
			this.fruitType = 7;
		}else {
			this.fruitType = 1;
		}
		
		aFruit = new Animation(50, Assets.help);
		aDFruit = new Animation(50, Assets.dHelp);
		aPortal = new Animation(50, Assets.portal);
		
		timer = 0;
		lastTime = System.currentTimeMillis();
	}
	
	public int generateSpawnPos() {
		
		this.possibleSpawnPos.clear();
		
		for(int i = 0;i < 375;i++) {
			this.possibleSpawnPos.add(i);
		}
		
		//Removing walls pos
		for(int i = 0;i < 5;i++) {
			this.possibleSpawnPos.set(((i) * 25  + 17), -1);
			this.possibleSpawnPos.set(((i + 10) * 25  + 7), -1);
		}
		//Removing player pos
		for(int i = 0;i < player.getBodyXPosition().size();i++) {
			this.possibleSpawnPos.set((((this.player.getBodyYPosition().get(i) / Entity.WIDTH)) * 25 + this.player.getBodyXPosition().get(i) / Entity.WIDTH), -1);
		}
		//Removing area around the head
		this.possibleSpawnPos.set((((int) this.player.getY() / Entity.WIDTH) * 25 + ((int) this.player.getX() / Entity.WIDTH)), -1);
		
		if(this.player.getY() < 440) {
//			System.out.println(this.player.getX());
//			System.out.println(this.player.getY());
			this.possibleSpawnPos.set((((int) this.player.getY() / Entity.WIDTH + 1) * 25 + ((int) this.player.getX() / Entity.WIDTH)), -1);
		}
		
		if(this.player.getY() > 0) {
//			System.out.println(this.player.getX());
//			System.out.println(this.player.getY());
			this.possibleSpawnPos.set((((int) this.player.getY() / Entity.WIDTH - 1) * 25 + ((int) this.player.getX() / Entity.WIDTH)), -1);
		}
		
		if(this.player.getX() < 760) {
//			System.out.println(this.player.getX());
//			System.out.println(this.player.getY());
			this.possibleSpawnPos.set((((int) this.player.getY() / Entity.WIDTH) * 25 + ((int) this.player.getX() / Entity.WIDTH) + 1), -1);
		}
		
		if(this.player.getX() > 0) {
//			System.out.println(this.player.getX());
//			System.out.println(this.player.getY());
			this.possibleSpawnPos.set((((int) this.player.getY() / Entity.WIDTH) * 25 + ((int) this.player.getX() / Entity.WIDTH) - 1), -1);
		}
			
		int i = 0;
		
		while(i < possibleSpawnPos.size()) {
			if(possibleSpawnPos.get(i) == -1) {
				possibleSpawnPos.remove(i);
			}else {
				i++;
			}
		}
		
		
		
		return possibleSpawnPos.get(random.nextInt(this.possibleSpawnPos.size()));
	}
	
	public void fruitCollision() {
		
		if(player.getX() == this.x && player.getY() == this.y) {
			fruitCollision = true;
			
			switch(this.fruitType) {
				case 1:
//					System.out.println("normal");
					player.setScore(player.getScore() + 1 * player.scoreMultiplier());
					this.player.bodyXPosition.add(0, (int) x);
					this.player.bodyYPosition.add(0, (int) y);
					break;
				case 2:
//					System.out.println("star");
					player.setScore(player.getScore() + 1 * player.scoreMultiplier());
					player.setSnakeType(2);
					break;
				case 3:
//					System.out.println("kitty");
					player.setScore(player.getScore() + 1 * player.scoreMultiplier());
					player.setSnakeType(3);
					break;
				case 4:
//					System.out.println("teleport");
					player.setScore(player.getScore() + 1 * player.scoreMultiplier());
					player.setSnakeType(4);
					break;
				case 5:
//					System.out.println("double");
					player.setScore(player.getScore() + 2 * player.scoreMultiplier());
					this.player.bodyXPosition.add(0, (int) x);
					this.player.bodyYPosition.add(0, (int) y);
					break;
				case 6:
//					System.out.println("shrink");
					int aux = 2;
					for(int i = this.player.getBodyXPosition().size();i > aux;i--) {
						this.player.bodyXPosition.remove(this.player.bodyXPosition.size()-1);
						this.player.bodyYPosition.remove(this.player.bodyYPosition.size()-1);
					}
					break;
				case 7:
//					System.out.println("death");
					this.player.gameOver = true;
					break;
			}
		}
	}

	@Override
	public void tick() {
		
		this.fruitCollision();
		aFruit.tick();
		aDFruit.tick();
		aPortal.tick();
		
		timer += System.currentTimeMillis() - lastTime;
		lastTime = System.currentTimeMillis();
		
		if(timer >= fruitTimer || fruitCollision == true) {
			timer = 0;
			fruitCollision = false;
			
			int aux = generateSpawnPos();
			this.x =(float) (aux % 25) * Entity.WIDTH;
			this.y =(float) (aux / 25) * Entity.WIDTH;
			
			aux = random.nextInt(100);
			if(aux < 50) {
				this.fruitType = 1;
			}else if(aux < 59) {
				this.fruitType = 2;
			}else if(aux < 68) {
				this.fruitType = 3;
			}else if(aux < 76) {
				this.fruitType = 4;
			}else if(aux < 84) {
				this.fruitType = 5;
			}else if(aux < 92) {
				this.fruitType = 6;
			}else if(aux < 100) {
				this.fruitType = 7;
			}else {
				this.fruitType = 1;
			}
		}
		
	}

	@Override
	public void render(Graphics g) {
		
		
		
		switch(this.fruitType) {
		case 1:
			g.drawImage(aFruit.getCurrentFrame(), (int) x, (int) y, null);
			break;
		case 2:
			g.drawImage(Assets.star, (int) x, (int) y, null);
			break;
		case 3:
			g.drawImage(Assets.kitty, (int) x, (int) y, null);
			break;
		case 4:
			g.drawImage(aPortal.getCurrentFrame(), (int) x, (int) y, null);
			break;
		case 5:
			g.drawImage(aDFruit.getCurrentFrame(), (int) x, (int) y, null);
			break;
		case 6:
			g.drawImage(Assets.shrink, (int) x, (int) y, null);
			break;
		case 7:
			g.drawImage(Assets.bomb, (int) x, (int) y, null);
			break;
		}
		
//		for(int i = 0;i < this.possibleSpawnPos.size();i++) {
//			
//			int aux = possibleSpawnPos.get(i);
//			
//			this.x =(float) (aux % 25) * Entity.WIDTH;
//			this.y =(float) (aux / 25) * Entity.WIDTH;(int i = 0;i < this.possibleSpawnPos.size();i++) {
//		
//		int aux = possibleSpawnPos.get(i);
//		
//		this.x =(float) (aux % 25) * Entity.WIDTH;
//		this.y =(float) (aux / 25) * Entity.WIDTH;
//		
//		g.drawImage(Assets.help[1], (int) x, (int) y, null);
//	}
//			
//			g.drawImage(Assets.help[1], (int) x, (int) y, null);
//		}
	}

}
