package hbgameentities;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import hbgame.Game;
import hbgame.Handler;
import hbgamegfx.Animation;
import hbgamegfx.Assets;
import hbgamestates.GameOverState;

public class Player extends Entity{
	
	// Animation
	
	private Animation aUp;
	private Animation aDown;
	private Animation aLeft;
	private Animation aRight;
	
	private Animation sUp;
	private Animation sDown;
	private Animation sLeft;
	private Animation sRight;
	
	private Animation kUp;
	private Animation kDown;
	private Animation kLeft;
	private Animation kRight;
	
	private Animation pUp;
	private Animation pDown;
	private Animation pLeft;
	private Animation pRight;
	
	private Animation aBody;
	
	protected long lastTime, timer;
	private final int powerUpTimer = 10000; // 10 seconds

	protected ArrayList<Integer> bodyXPosition;
	protected ArrayList<Integer> bodyYPosition;
	
	public ArrayList<Integer> getBodyXPosition() {
		return bodyXPosition;
	}

	public void setBodyXPosition(ArrayList<Integer> bodyXPosition) {
		this.bodyXPosition = bodyXPosition;
	}

	public ArrayList<Integer> getBodyYPosition() {
		return bodyYPosition;
	}

	public void setBodyYPosition(ArrayList<Integer> bodyYPosition) {
		this.bodyYPosition = bodyYPosition;
	}

	private int snakeType = 1;
	private long score = 0;
	protected float xMove, yMove;
	protected float speed = 32;
	protected boolean gameOver = false;
	
	public Player(Handler handler, float x, float y) {
		super(handler, x, y, Entity.WIDTH, Entity.HEIGHT);
		
		bodyXPosition = new ArrayList<Integer>();
		bodyYPosition = new ArrayList<Integer>();
		
		this.bodyXPosition.add((int) (x - (this.bodyXPosition.size() + 1) * Entity.WIDTH));
		this.bodyXPosition.add((int) (x - (this.bodyXPosition.size() + 1) * Entity.WIDTH));
		this.bodyYPosition.add((int) y);
		this.bodyYPosition.add((int) y);
		
		aBody = new Animation(200, Assets.floater);
		
		aUp = new Animation(200, Assets.boatUp);
		aDown = new Animation(200, Assets.boatDown);
		aLeft = new Animation(200, Assets.boatLeft);
		aRight = new Animation(200, Assets.boatRight);
		
		sUp = new Animation(200, Assets.starUp);
		sDown = new Animation(200, Assets.starDown);
		sLeft = new Animation(200, Assets.starLeft);
		sRight = new Animation(200, Assets.starRight);
		
		kUp = new Animation(200, Assets.kittyUp);
		kDown = new Animation(200, Assets.kittyDown);
		kLeft = new Animation(200, Assets.kittyLeft);
		kRight = new Animation(200, Assets.kittyRight);
		
		pUp = new Animation(200, Assets.portalUp);
		pDown = new Animation(200, Assets.portalDown);
		pLeft = new Animation(200, Assets.portalLeft);
		pRight = new Animation(200, Assets.portalRight);
		
		lastTime = System.currentTimeMillis();
		
		xMove = 0;
		yMove = 0;
	}
	
	public void move() {
		
		//RAN INTO ITSELF
		for(int i = 0;i < this.bodyXPosition.size();i++) {
			for(int j = 0;j < this.bodyYPosition.size();j++) {
				if(x+xMove == this.bodyXPosition.get(i) && y+yMove == this.bodyYPosition.get(i)) {
					this.gameOver = true;
				}
			}
		}
		
		//RAN INTO WALL
			
		if(this.snakeType != 3)
		{
			for(int i = 0;i < 5;i++) {
				
				if(x+xMove == 17 * Entity.WIDTH && y+yMove == i * Entity.WIDTH) {
					this.gameOver = true;
				}
				
				if(x+xMove == 7 * Entity.WIDTH && y+yMove == (10 + i) * Entity.WIDTH) {
					this.gameOver = true;
				}
					
			}
		}
		
		if(xMove != 0 || yMove != 0) {
			this.bodyXPosition.add(0, (int) x);
			this.bodyYPosition.add(0, (int) y);
			this.bodyXPosition.remove(this.bodyXPosition.size()-1);
			this.bodyYPosition.remove(this.bodyYPosition.size()-1);
		}
		
		//OUT OF BOUNDS
		if(x+xMove < 0 || x+xMove > 768 || y+yMove < 0 || y+yMove > 448) {
			
			if(this.snakeType == 4) {
				if(x+xMove < 0) {
					x = 768;
				}else if(x+xMove > 768) {
					x = 0;
				}else if(y+yMove < 0) {
					y = 448;
				}else {
					y = 0;
				}
			}else {
				this.gameOver = true;
			}
		}else {
			x += xMove;
			y += yMove;
		}
		
	}
	
	public float getxMove() {
		return xMove;
	}

	public void setxMove(float xMove) {
		this.xMove = xMove;
	}

	public float getyMove() {
		return yMove;
	}

	public void setyMove(float yMove) {
		this.yMove = yMove;
	}
	
	public long getScore() {
		return score;
	}

	public void setScore(long score) {
		this.score = score;
	}
	
	public void getInput() {
		
//		SMOOTH MOVEMENT TEST
//		if(x % 32 == 0 && y % 32 == 0) {
			if(handler.getKeyManager().up && yMove != speed) {
				yMove = -speed;
				xMove = 0;
			}
			if(handler.getKeyManager().down && yMove != -speed) {
				yMove = speed;
				xMove = 0;
			}
			if(handler.getKeyManager().left && xMove != speed) {
				xMove = -speed;
				yMove = 0;
			}
			if(handler.getKeyManager().right && xMove != -speed) {
				xMove = speed;
				yMove = 0;
			}
//		}
	}

	@Override
	public void tick() {
		
		timer += System.currentTimeMillis() - lastTime;
		lastTime = System.currentTimeMillis();
		
		if(timer >= powerUpTimer) {
			timer = 0;
			this.setSnakeType(1);
		}
		
		aUp.tick();
		aDown.tick();
		aLeft.tick();
		aRight.tick();
		aBody.tick();
		
		sUp.tick();
		sDown.tick();
		sLeft.tick();
		sRight.tick();
		
		kUp.tick();
		kDown.tick();
		kLeft.tick();
		kRight.tick();
		
		pUp.tick();
		pDown.tick();
		pLeft.tick();
		pRight.tick();
		
		getInput();
		move();
	}
	
	

	public boolean isGameOver() {
		return gameOver;
	}

	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}

	@Override
	public void render(Graphics g) {
		
		g.drawImage(getCurrentAnimationFrame(), (int) x, (int) y, this.width, this.height, null);
		
		for(int i = 0;i < this.bodyXPosition.size();i++)
		{
			g.drawImage(aBody.getCurrentFrame(), this.bodyXPosition.get(i), this.bodyYPosition.get(i), this.width, this.height, null);
		}
		
		g.setColor(Color.white);
		g.setFont(new Font("arial", Font.BOLD, 16));
		g.drawString("Score: " + Long.toString(score),  0, 480);
	}
	
	private BufferedImage getCurrentAnimationFrame() {
		
		if(yMove > 0) {
			switch(this.snakeType) {
			case 1:
				return aDown.getCurrentFrame();
			case 2:
				return sDown.getCurrentFrame();
			case 3:
				return kDown.getCurrentFrame();
			case 4:
				return pDown.getCurrentFrame();
			}
		}else if(xMove < 0) {
			switch(this.snakeType) {
			case 1:
				return aLeft.getCurrentFrame();
			case 2:
				return sLeft.getCurrentFrame();
			case 3:
				return kLeft.getCurrentFrame();
			case 4:
				return pLeft.getCurrentFrame();
			}
		}else if(yMove < 0) {
			switch(this.snakeType) {
			case 1:
				return aUp.getCurrentFrame();
			case 2:
				return sUp.getCurrentFrame();
			case 3:
				return kUp.getCurrentFrame();
			case 4:
				return pUp.getCurrentFrame();
			}
		}else {
			switch(this.snakeType) {
			case 1:
				return aRight.getCurrentFrame();
			case 2:
				return sRight.getCurrentFrame();
			case 3:
				return kRight.getCurrentFrame();
			case 4:
				return pRight.getCurrentFrame();
			}
		}
		return null;
	}

	public int scoreMultiplier() {
		if(this.snakeType == 2)
			return 2;
		else
			return 1;
	}

	public void setSnakeType(int index) {
		this.snakeType = index;
		timer = 0;
	}

}
