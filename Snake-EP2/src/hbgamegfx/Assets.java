package hbgamegfx;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

//  

public class Assets {
	
	private static final int width = 32, height = 32;
	
	public static BufferedImage[] sea;
	
	public static BufferedImage[] boatUp;
	public static BufferedImage[] boatDown;
	public static BufferedImage[] boatLeft;
	public static BufferedImage[] boatRight;
	
	public static BufferedImage[] starUp;
	public static BufferedImage[] starDown;
	public static BufferedImage[] starLeft;
	public static BufferedImage[] starRight;
	
	public static BufferedImage[] kittyUp;
	public static BufferedImage[] kittyDown;
	public static BufferedImage[] kittyLeft;
	public static BufferedImage[] kittyRight;
	
	public static BufferedImage[] portalUp;
	public static BufferedImage[] portalDown;
	public static BufferedImage[] portalLeft;
	public static BufferedImage[] portalRight;
	
	public static BufferedImage[] floater;
	public static BufferedImage[] help;
	public static BufferedImage[] dHelp;
	public static BufferedImage[] portal;
	
	public static BufferedImage kitty;
	public static BufferedImage star;
	public static BufferedImage shrink;
	public static BufferedImage bomb;
	public static BufferedImage wall;
	public static BufferedImage logo;
	
	public static String generalPath;
	public static String path;
	
	public static void init() {
		
//		generalPath = System.getProperty("user.dir");
//		generalPath = generalPath + "/res/textures/";
//		
//		path = generalPath + "hugoSpriteSheet.png";
//		SpriteSheet sheet1 = new SpriteSheet(ImageLoader.loadImage(path));
//		
//		path = generalPath + "oceanSpriteSheet.png";
//		SpriteSheet sheet2 = new SpriteSheet(ImageLoader.loadImage(path));
//		
//		SpriteSheet sheet1 = new SpriteSheet(ImageLoader.loadImage("/home/hugo/hugo/Studies/oo/ep2/Snake-EP2/res/textures/hugoSpriteSheet.png"));
//		SpriteSheet sheet2 = new SpriteSheet(ImageLoader.loadImage("/home/hugo/hugo/Studies/oo/ep2/Snake-EP2/res/textures/oceanSpriteSheet.png"));
//		SpriteSheet sheet3 = new SpriteSheet(ImageLoader.loadImage("/home/hugo/hugo/Studies/oo/ep2/Snake-EP2/res/textures/powerUpsSpriteSheet.png"));
//		
//		logo = ImageLoader.loadImage("/home/hugo/hugo/Studies/oo/ep2/Snake-EP2/res/textures/logo.png");
		
		SpriteSheet sheet1 = new SpriteSheet(ImageLoader.loadImage("./res/textures/hugoSpriteSheet.png"));
		SpriteSheet sheet2 = new SpriteSheet(ImageLoader.loadImage("./res/textures/oceanSpriteSheet.png"));
		SpriteSheet sheet3 = new SpriteSheet(ImageLoader.loadImage("./res/textures/powerUpsSpriteSheet.png"));
		
		logo = ImageLoader.loadImage("./res/textures/logo.png");
		
//		SpriteSheet sheet1 = new SpriteSheet(ImageLoader.loadImage("./hugoSpriteSheet.png"));
//		SpriteSheet sheet2 = new SpriteSheet(ImageLoader.loadImage("./oceanSpriteSheet.png"));
//		SpriteSheet sheet3 = new SpriteSheet(ImageLoader.loadImage("./powerUpsSpriteSheet.png"));
		
		sea = new BufferedImage[16];
		
		boatUp = new BufferedImage[2];
		boatDown = new BufferedImage[2];
		boatLeft = new BufferedImage[2];
		boatRight = new BufferedImage[2];
		
		starUp = new BufferedImage[2];
		starDown = new BufferedImage[2];
		starLeft = new BufferedImage[2];
		starRight = new BufferedImage[2];
		
		kittyUp = new BufferedImage[2];
		kittyDown = new BufferedImage[2];
		kittyLeft = new BufferedImage[2];
		kittyRight = new BufferedImage[2];
		
		portalUp = new BufferedImage[2];
		portalDown = new BufferedImage[2];
		portalLeft = new BufferedImage[2];
		portalRight = new BufferedImage[2];
		
		floater = new BufferedImage[2];
		help = new BufferedImage[2];
		dHelp = new BufferedImage[2];
		portal = new BufferedImage[2];
		
		starUp[0] = sheet3.crop(0, 0, width, height);
		starUp[1] = sheet3.crop(0, height, width, height);
		starDown[0] = sheet3.crop(width, 0, width, height);
		starDown[1] = sheet3.crop(width, height, width, height);
		starLeft[0] = sheet3.crop(2 * width, 0, width, height);
		starLeft[1] = sheet3.crop(2 * width, height, width, height);
		starRight[0] = sheet3.crop(3 * width, 0, width, height);
		starRight[1] = sheet3.crop(3 * width, height, width, height);
		
		kittyUp[0] = sheet3.crop(0, 2 * height, width, height);
		kittyUp[1] = sheet3.crop(0, 3 * height, width, height);
		kittyDown[0] = sheet3.crop(width, 2 * height, width, height);
		kittyDown[1] = sheet3.crop(width, 3 * height, width, height);
		kittyLeft[0] = sheet3.crop(2 * width, 2 * height, width, height);
		kittyLeft[1] = sheet3.crop(2 * width, 3 * height, width, height);
		kittyRight[0] = sheet3.crop(3 * width, 2 * height, width, height);
		kittyRight[1] = sheet3.crop(3 * width, 3 * height, width, height);
		
		portalUp[0] = sheet3.crop(0, 4 * height, width, height);
		portalUp[1] = sheet3.crop(0, 5 * height, width, height);
		portalDown[0] = sheet3.crop(width, 4 * height, width, height);
		portalDown[1] = sheet3.crop(width, 5 * height, width, height);
		portalLeft[0] = sheet3.crop(2 * width, 4 * height, width, height);
		portalLeft[1] = sheet3.crop(2 * width, 5 * height, width, height);
		portalRight[0] = sheet3.crop(3 * width, 4 * height, width, height);
		portalRight[1] = sheet3.crop(3 * width, 5 * height, width, height);
		
		boatUp[0] = sheet1.crop(0, 0, width, height);
		boatUp[1] = sheet1.crop(0, height, width, height);
		boatDown[0] = sheet1.crop(width, 0, width, height);
		boatDown[1] = sheet1.crop(width, height, width, height);
		boatLeft[0] = sheet1.crop(2 * width, 0, width, height);
		boatLeft[1] = sheet1.crop(2 * width, height, width, height);
		boatRight[0] = sheet1.crop(3 * width, 0, width, height);
		boatRight[1] = sheet1.crop(3 * width, height, width, height);
		
		floater[0] =  sheet1.crop(0, 2 * height, width, height);
		floater[1] =  sheet1.crop(width, 2 * height, width, height);
		
		help[0] =  sheet1.crop(2 * width, 2 * height, width, height);
		help[1] =  sheet1.crop(3 * width, 2 * height, width, height);
		
		dHelp[0] = sheet1.crop(2 * width, 4 * height, width, height);
		dHelp[1] = sheet1.crop(3 * width, 4 * height, width, height);
		
		portal[0] = sheet1.crop(0, 4 * height, width, height);
		portal[1] = sheet1.crop(width, 4 * height, width, height);
		
		kitty = sheet1.crop(width, 5 * height, width, height);
		star = sheet1.crop(2 * width, 5 * height, width, height);
		shrink = sheet1.crop(3 * width, 5 * height, width, height);
		bomb = sheet1.crop(0, 5 * height, width, height);
		wall = sheet1.crop(0, 3 * height, width, height);
		
		sea[0] = sheet2.crop(0, 0, width, height);
		sea[1] = sheet2.crop(width, 0, width, height);
		sea[2] = sheet2.crop(2*width, 0, width, height);
		sea[3] = sheet2.crop(3*width, 0, width, height);
		sea[4] = sheet2.crop(4*width, 0, width, height);
		sea[5] = sheet2.crop(5*width, 0, width, height);
		sea[6] = sheet2.crop(6*width, 0, width, height);
		sea[7] = sheet2.crop(7*width, 0, width, height);
		sea[8] = sheet2.crop(0, height, width, height);
		sea[9] = sheet2.crop(width, height, width, height);
		sea[10] = sheet2.crop(2*width, height, width, height);
		sea[11] = sheet2.crop(3*width, height, width, height);
		sea[12] = sheet2.crop(4*width, height, width, height);
		sea[13] = sheet2.crop(5*width, height, width, height);
		sea[14] = sheet2.crop(6*width, height, width, height);
		sea[15] = sheet2.crop(7*width, height, width, height);
		
	}

}
