package hbgameinput;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyManager implements KeyListener{
	
	private boolean[] keys;
	private int lastKeyCode = KeyEvent.VK_D;
	public boolean up, down, left, right, enter, r, c, m, p, t;
	
	public KeyManager() {
		keys = new boolean[256];
	}
	
	public void tick() {
		up = keys[KeyEvent.VK_W] || keys[KeyEvent.VK_UP];
		down = keys[KeyEvent.VK_S] || keys[KeyEvent.VK_DOWN];
		left = keys[KeyEvent.VK_A] || keys[KeyEvent.VK_LEFT];
		right = keys[KeyEvent.VK_D] || keys[KeyEvent.VK_RIGHT];
		
		enter = keys[KeyEvent.VK_ENTER];
		r = keys[KeyEvent.VK_R];
		c = keys[KeyEvent.VK_C];
		m = keys[KeyEvent.VK_M];
		p = keys[KeyEvent.VK_P];
		t = keys[KeyEvent.VK_T];
	}

	@Override
	public void keyPressed(KeyEvent e) {
		keys[lastKeyCode] = false;
		lastKeyCode = e.getKeyCode();
		keys[e.getKeyCode()] = true;
//		System.out.println("Pressed!");
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}
	
}
