package hbgame;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

import hbgamedisplay.Display;
import hbgameentities.Player;
import hbgamegfx.Animation;
import hbgamegfx.Assets;
import hbgamegfx.ImageLoader;
import hbgamegfx.SpriteSheet;
import hbgameinput.KeyManager;
import hbgamestates.CreditsState;
import hbgamestates.GameOverState;
import hbgamestates.GameState;
import hbgamestates.MenuState;
import hbgamestates.RankingState;
import hbgamestates.State;
import hbgamestates.TutorialState;

public class Game implements Runnable {
	
	public long finalScore = 0;
	
	// Display
	private Display display;
	private int width, height;
	public String title;
	
	// Thread
	private boolean running = false;
	private Thread thread;
	
	// Graphics
	private BufferStrategy bs;
	private Graphics g;
	
	//States
	private State gameState;
	private State menuState;
	private State gameOverState;
	private State rankingState;
	private State creditsState;
	private State tutorialState;
	
	// Input
	private KeyManager keyManager;
	
	//Handler
	private Handler handler;
	
	public Game(String title, int width, int height) {
		this.width =  width;
		this.height = height;
		this.title = title;
		keyManager = new KeyManager();
	}
	
	public void init() {
		display = new Display(title, width, height);
		display.getFrame().addKeyListener(keyManager);
		Assets.init();
		
		handler = new Handler(this);
		
		gameState = new GameState(handler);
		menuState = new MenuState(handler);
		gameOverState = new GameOverState(handler);
		rankingState = new RankingState(handler);
		creditsState = new CreditsState(handler);
		tutorialState = new TutorialState(handler);
		
		State.setState(menuState);
	}
	
	private void tick() {
		if(State.getState() != null)
			State.getState().tick();
	}
	
	private void render() {
		keyManager.tick();
		
		bs = display.getCanvas().getBufferStrategy();
		if(bs == null) {
			display.getCanvas().createBufferStrategy(3);
			return;
		}
		g = bs.getDrawGraphics();
		
		g.clearRect(0, 0, width, height);
		
		if(State.getState() != null)
			State.getState().render(g);
		
		bs.show();
		g.dispose();;
	}
	
	public void run() {
		init();
		
		int fpsTick = 7;
		int fpsRender = 1000;
		double timePerTick = 1000000000 / fpsTick;
		double timePerRender = 1000000000 / fpsRender;
		double deltaRender = 0;
		double deltaTick = 0;
		long now;
		long lastTime = System.nanoTime();
		
		while (running) {
			now = System.nanoTime();
			deltaTick += (now - lastTime) / timePerTick;
			deltaRender+= (now - lastTime) / timePerRender;
			lastTime = now;
		   
			if(deltaTick >= 1) {
				tick();
				deltaTick = 0;
			}
			if(deltaRender >= 1) {
				render();
				deltaRender = 0;
			}
			try {
				Thread.sleep(1);
			}catch(InterruptedException e) {
				e.printStackTrace();
			}
		  
		}
		
		stop();
	}
	
	public KeyManager getKeyManager() {
		return keyManager;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public synchronized void start() {
		
		if(running) return;
		running = true;
		
		thread = new Thread(this);
		thread.start(); //calls run method
	}
	public synchronized void stop() {
		
		if(!running) return;
		running = false;
		
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

	public State getGameState() {
		return gameState;
	}

	public void setGameState(State gameState) {
		this.gameState = gameState;
	}
	
	public void setGameOverState(State gameOverState) {
		this.gameOverState = gameOverState;
	}
	
	public void setMenuState(State menuState) {
		this.menuState = menuState;
	}
	
	public void setRankingState(State rankingState) {
		this.rankingState = rankingState;
	}
	
	public void setCreditsState(State creditsState) {
		this.creditsState = creditsState;
	}
	
}