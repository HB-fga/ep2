package hbgameworlds;

import java.awt.Graphics;

import hbgame.Game;
import hbgame.Handler;
import hbgamegfx.Animation;
import hbgamegfx.Assets;
import hbgametiles.Tile;
import hbgameutils.Utils;

public class World {
	
	private Animation seaAnimation;
	private Handler handler;
	private int width, height;
	private int[][] tiles;
	
	public World(Handler handler, String path) {
		this.handler = handler;
		loadWorld(path);
		seaAnimation = new Animation(100, Assets.sea);
	}
	
	public void tick() {
		seaAnimation.tick();
	}
	
	public void render(Graphics g) {
		for(int y = 0;y < height;y++) {
			for(int x = 0;x < width;x++) {
				g.drawImage(seaAnimation.getCurrentFrame(), x * Tile.WIDTH, y * Tile.HEIGHT, null);
				
				if(getTile(x, y).getId() != 0)
					getTile(x, y).render(g, x * Tile.WIDTH, y * Tile.HEIGHT);
					
					
			}
		}
		
		
	}
	
	public Tile getTile(int x, int y) {
		Tile t = Tile.tiles[tiles[x][y]];
		if(t == null) {
			return Tile.seaTile;
		}
		return t;
	}
	
	private void loadWorld(String path) {
		String file = Utils.loadFileAsString(path);
		String[] tokens = file.split("\\s+");
		width = Utils.parseInt(tokens[0]);
		height = Utils.parseInt(tokens[1]);
		
		tiles = new int[width][height];
		for(int y = 0;y < height;y++) {
			for(int x = 0;x < width;x++) {
				tiles[x][y] = Utils.parseInt(tokens[(x + y * width) + 2]);
			}
		}
	}
}
